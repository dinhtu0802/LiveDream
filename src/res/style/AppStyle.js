import {
    Platform
} from 'react-native';
import {sizeFont} from "../../utils/Size";

export const APP_COLOR = '#E51759';
export const APP_COLOR_PINK = '#FF6A96';
export const APP_BACKGROUND = '#30375C';
export const SEPERATOR_COLOR = '#FAFAFA';
export const APP_TEXT_COLOR = '#545454';
export const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0;
export const COLOR_BUTTON = '#57586b';
export const TOOL_BAR_TEXT = {
    color: '#fff',
    fontSize: sizeFont(4.8),
};