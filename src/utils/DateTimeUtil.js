import React from 'react';
import {fourIntegerDigits, twoIntegerDigits} from "./FormatUtil";

export const MM_DD_YYYY_HH_MM_SS = 'MM/DD/YYYY hh:mm:ss';
export const YYYY_MM_DD = 'YYYY-MM-DD';

export default class DateTimeUtil {

    static dateToString(date: Date, format: string) {
        const year = date.getFullYear();
        const month = date.getMonth() + 1;
        const day = date.getDate();
        const hour = date.getHours();
        const minute = date.getMinutes();
        const second = date.getSeconds();

        return format.replace('YYYY', fourIntegerDigits(year))
            .replace('MM', twoIntegerDigits(month))
            .replace('DD', twoIntegerDigits(day))
            .replace('hh', twoIntegerDigits(hour))
            .replace('mm', twoIntegerDigits(minute))
            .replace('ss', twoIntegerDigits(second))
    };

    static convertDateToStringYYYYmmDDhhMMss(date) {
        if (!date) return;
        year = "" + date.getFullYear();
        month = "" + (date.getMonth() + 1);
        if (month.length === 1) {
            month = "0" + month;
        }
        day = "" + date.getDate();
        if (day.length === 1) {
            day = "0" + day;
        }
        hour = "" + date.getHours();
        if (hour.length === 1) {
            hour = "0" + hour;
        }
        minute = "" + date.getMinutes();
        if (minute.length === 1) {
            minute = "0" + minute;
        }
        second = "" + date.getSeconds();
        if (second.length === 1) {
            second = "0" + second;
        }
        return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    }

    static convertDateToStringYYYYmmDD(date) {
        if (!date) return;
        year = "" + date.getFullYear();
        month = "" + (date.getMonth() + 1);
        if (month.length === 1) {
            month = "0" + month;
        }
        day = "" + date.getDate();
        if (day.length === 1) {
            day = "0" + day;
        }
        return year + "-" + month + "-" + day;
    }

    static convertDateToStringYYYYmmDD_H(date: Date) {
        if (!date) return;
        year = "" + date.getFullYear();
        month = "" + (date.getMonth() + 1);
        if (month.length === 1) {
            month = "0" + month;
        }
        day = "" + date.getDate();
        if (day.length === 1) {
            day = "0" + day;
        }
        hour = "" + date.getHours();
        if (hour.length === 1) {
            hour = "0" + hour;
        }
        return year + month + day + '-' + hour;
    }

    static getTimeOfDate(date) {
        if (!date) return;
        hour = "" + date.getHours();
        if (hour.length === 1) {
            hour = "0" + hour;
        }
        minute = "" + date.getMinutes();
        if (minute.length === 1) {
            minute = "0" + minute;
        }
        second = "" + date.getSeconds();
        if (second.length === 1) {
            second = "0" + second;
        }
        return hour + ":" + minute + ":" + second;
    }

    static stringToString(string, fromFormat: string, toFormat: string) {
        const year = string.substr(fromFormat.indexOf('YYYY'), 4);
        const month = string.substr(fromFormat.indexOf('MM'), 2);
        const day = string.substr(fromFormat.indexOf('DD'), 2);
        const hour = string.substr(fromFormat.indexOf('hh'), 2);
        const minute = string.substr(fromFormat.indexOf('mm'), 2);
        const second = string.substr(fromFormat.indexOf('ss'), 2);
        return toFormat.replace('YYYY', fourIntegerDigits(year))
            .replace('MM', twoIntegerDigits(month))
            .replace('DD', twoIntegerDigits(day))
            .replace('hh', twoIntegerDigits(hour))
            .replace('mm', twoIntegerDigits(minute))
            .replace('ss', twoIntegerDigits(second))
    };

}