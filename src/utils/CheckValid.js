import DialogUtil from "./DialogUtil";

export function isValidPassphrase(pw1, pw2) {
    let result = false;
    if (pw1 && pw2) {
        if (pw1.length < 5) {
            DialogUtil.showMessageDialog('Error', 'Passphrase too short. Minimal 5 characters.', 'OK')
        } else if (pw1 !== pw2) {
            DialogUtil.showMessageDialog('Error', 'Passphrases do not match', 'OK')
        } else {
            result = true
        }
    } else {
        DialogUtil.showMessageDialog('Error', 'Passphrases cannot be empty', 'OK')
    }
    return result
}
