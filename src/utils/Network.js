import {NetInfo} from "react-native";
const CHANGE_EVENT = 'change';

export const isConnect = () => {
    return NetInfo.isConnected.fetch().then(isConnected => {
        return isConnected;
    });
};

export const addConnectionListener = (listener) => {
    NetInfo.isConnected.addEventListener(
        CHANGE_EVENT,
        listener
    );
};

export const removeConnectionListener = (listener) => {
    NetInfo.isConnected.removeEventListener(
        CHANGE_EVENT,
        listener
    );
};