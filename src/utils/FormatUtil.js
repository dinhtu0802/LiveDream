export const twoIntegerDigits = (value: number) => {
    return ("0" + value).slice(-2);
};

export const fourIntegerDigits = (value: number) => {
    return ("000" + value).slice(-4);
};