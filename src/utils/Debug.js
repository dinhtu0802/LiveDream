export const showAlert = (message) => {
    if (__DEV__ && message)
        alert(message)
};

export const showLog = (message) => {
    if (__DEV__ && message)
        console.log(message)
};