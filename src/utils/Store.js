import {
    AsyncStorage
} from 'react-native';

const KEY_ACCOUNT_ID = 'account_id';
const KEY_TOKEN = 'access_token';
const KEY_WALLET = 'access_wallet';
const KEY_BALANCE = 'access_balance';
const KEY_LANGUAGE = 'languages';
const FCM_TOKEN = 'fcm_token';
const KEY_PIN_CODE = 'access_pin_code';

export const saveFCMToken = (fCMToken) => AsyncStorage.setItem(FCM_TOKEN, fCMToken);

export const getFCMToken = () => {
    return AsyncStorage.getItem(FCM_TOKEN);
};

export const saveAccountId = (accountId) => AsyncStorage.setItem(KEY_ACCOUNT_ID, accountId);

export const getAccountId = () => {
    return AsyncStorage.getItem(KEY_ACCOUNT_ID);
};

export const removeAccountId = () => AsyncStorage.removeItem(KEY_ACCOUNT_ID);

export const saveToken = (token) => AsyncStorage.setItem(KEY_TOKEN, token);
export const removeToken = () => AsyncStorage.removeItem(KEY_TOKEN);

export const savePinCode = (pincode) => AsyncStorage.setItem(KEY_PIN_CODE, pincode);

export const getPinCode = () => {
    return AsyncStorage.getItem(KEY_PIN_CODE);
};

export const getToken = () => {
    return AsyncStorage.getItem(KEY_TOKEN);
};
export const getDataWallet = () => {
    return AsyncStorage.getItem(KEY_WALLET);
};

export const saveDataWallet = (wallet) => {
    AsyncStorage.setItem(KEY_WALLET, wallet);
};
export const removeWallet = () => AsyncStorage.removeItem(KEY_WALLET);

export const getDataBalance = () => {
    return AsyncStorage.getItem(KEY_BALANCE);
};

export const saveDataBalance = (balance) => {
    AsyncStorage.setItem(KEY_BALANCE, balance);
};
export const removeBalance = () => AsyncStorage.removeItem(KEY_BALANCE);

export const getLanguage = () => {
    return AsyncStorage.getItem(KEY_LANGUAGE);
};

export const saveLanguage = (languages) => {
    AsyncStorage.setItem(KEY_LANGUAGE, languages);
};
