import React, {Component} from 'react';
import {
    Image,
    StyleSheet,
    Text, TouchableOpacity, TouchableWithoutFeedback,
    View, Animated, FlatList, ActivityIndicator
} from 'react-native';

import Swiper from 'react-native-swiper';
import Video from "react-native-video";
import * as Animatable from 'react-native-animatable';
import {sizeFont, sizeHeight, sizeWidth} from "./src/utils/Size";
import {APP_COLOR} from "./src/res/style/AppStyle";


const styles = StyleSheet.create({
    wrapper: {},
    slide1: {
        flex: 1,
        backgroundColor: 'white'
    },
    opacityWrap: {
        position: "absolute",
        backgroundColor: "#000000",
        opacity: 0.6,
        width: sizeWidth(100),
        height: sizeHeight(100)
    },
    backgroundVideo: {
        flex: 1
    },
    listLanguage: {
        position: "absolute",
        bottom: sizeWidth(0),
        backgroundColor: "black",
        paddingHorizontal: sizeWidth(3),
        width: '100%',
        opacity: 0.6,

    },
    textItem: {
        fontSize: sizeFont(4),
        color: "black",
        textAlign: "center",
    },
    itemCoin: {
        width: sizeWidth(16),
        height: sizeWidth(16),
        borderRadius: sizeWidth(8),
        backgroundColor: 'yellow',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: sizeWidth(3.5),
        marginVertical: sizeWidth(2)
    }
});

let arrayVideo = [
    "https://vjs.zencdn.net/v/oceans.mp4",
    "https://vjs.zencdn.net/v/oceans.mp4",
    "https://vjs.zencdn.net/v/oceans.mp4",
    "http://www.exit109.com/~dnn/clips/RW20seconds_1.mp4",
    "http://www.exit109.com/~dnn/clips/RW20seconds_1.mp4",


];
let LIST_COIN = [
    "5",
    "5",
    "5",
    "5",
    "5",
    "5",
    "5",
    "5",
    "5",
    "5",
    "5",
    "5",
];

export default class App extends Component {
    handleViewRef = ref => this.view = ref;

    constructor(props) {
        super(props);
        this.state = {
            pause: false,
            selectedIndex: 0,
            slideAnim: new Animated.Value(-220),
            showList: false,
            scrollEnabled: true
        }
    }

    render() {
        const {selectedIndex, scrollEnabled} = this.state;
        const {slideAnim} = this.state;
        let marginBottom = slideAnim;
        return (
            <Swiper
                scrollEnabled={scrollEnabled}
                automaticallyAdjustContentInsets={true}
                removeClippedSubviews={false}
                onIndexChanged={(index) =>
                {
                    this.setState({selectedIndex: index, showList: false})
                    console.log('ddsds')
                }
                    }
                loop={false}
                showsPagination={false}
                horizontal={false}
                index={0}
                style={styles.wrapper}
                showsButtons={false}
                loadMinimal={true}
                loadMinimalSize={1}
                loadMinimalLoader={this.loader()}
            >
                {arrayVideo.map((item, index) => {
                        {
                            if (selectedIndex === index) {
                                return (
                                    <View
                                        key={index}
                                        style={{flex: 1}}>
                                        <TouchableWithoutFeedback
                                            style={styles.backgroundVideo}
                                            onPress={() => this.setState({pause: !this.state.pause})}
                                        >
                                            <Video
                                                style={styles.backgroundVideo}
                                                paused={this.state.pause}
                                                ignoreSilentSwitch={"obey"}
                                                // posterResizeMode={'cover'}
                                                // poster={'https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/29571000_1230141807120375_8823436940434077405_n.jpg?_nc_cat=0&_nc_eui2=AeHNQFl0BpfwjJR30sZZxfmiVD27BWKY4ezE_H8qpfmSqltcA5Fw--C0UgqzDrb1bRzqETuhckt4vPr-cgYzxSlqoDGLll261PqjgzObeQ-OyA&oh=19ca7cc8599a4496b97c36ac916497df&oe=5BDAE888'}
                                                resizeMode={'cover'}
                                                repeat={true}
                                                source={{uri: item}}
                                                onBuffer={this.onBuffer}
                                                onEnd={this.onEnd}
                                                onError={(err) => alert(JSON.stringify(err))}/>
                                        </TouchableWithoutFeedback>
                                        <TouchableWithoutFeedback
                                            onPress={this.showListCoin}>
                                            <Image
                                                style={{
                                                    width: 30, height: 30, position: 'absolute',
                                                    bottom: 15,
                                                    right: 15
                                                }}
                                                source={require('./src/res/img/gif.gif')}
                                            />
                                        </TouchableWithoutFeedback>
                                        {/*{this.state.showList &&*/}
                                        {/*LIST_COIN.map((item, index) => {*/}
                                        {/*return (*/}
                                        {/*<Animated.View*/}
                                        {/*key={index}*/}
                                        {/*style={{*/}
                                        {/*position: "absolute",*/}
                                        {/*bottom: 0,*/}
                                        {/*marginBottom*/}
                                        {/*}}>*/}
                                        {/*<TouchableOpacity style={styles.itemCoin}>*/}
                                        {/*<Text style={styles.textItem}>{item}</Text>*/}
                                        {/*</TouchableOpacity>*/}
                                        {/*</Animated.View>*/}
                                        {/*)*/}
                                        {/*})*/}

                                        {/*}*/}

                                        {this.state.showList && this.renderListPicker()}

                                    </View>
                                )
                            }
                            else {
                                return (
                                    <View key={index}
                                          style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                        <Text style={{color: 'black', fontSize: 17}}>Loading...</Text>
                                    </View>
                                )
                            }
                        }
                    }
                )}
            </Swiper>

        );
    }

    loader = () => (
        <ActivityIndicator size="small" color="#00ff00"/>
    );

    renderItemLanguage = ({item}) => {
        return (
            <View>
                <TouchableOpacity style={styles.itemCoin}>
                    <Text style={styles.textItem}>{item}</Text>
                </TouchableOpacity>
            </View>
        );
    };


    renderListPicker = () => {
        const {slideAnim} = this.state;
        let marginBottom = slideAnim;
        return (
            <Animated.View style={{position: "absolute", top: 0, bottom: 0, left: 0, right: 0, marginBottom}}>
                {this.renderOpacity()}
                <FlatList
                    numColumns={4}
                    style={styles.listLanguage}
                    data={LIST_COIN}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={this.renderItemLanguage}
                />
            </Animated.View>
        );
    };

    renderOpacity = () => {
        return (
            <TouchableWithoutFeedback onPress={this.onCancelClick}>
                <View style={styles.opacityWrap}/>
            </TouchableWithoutFeedback>
        );
    };

    onCancelClick = () => {
        Animated.timing(this.state.slideAnim, {
            toValue: -220,
            duration: 150
        }).start(() => {
            this.setState({
                showList: false,
                scrollEnabled:true
            });
        });
    };
    showListCoin = () => {
        this.setState({
            showList: true,
            scrollEnabled:false
        });
        Animated.timing(this.state.slideAnim, {
            toValue: 0,
            duration: 250
        }).start();

    }
}